package HeppyFamily1;



import java.util.Arrays;
import java.util.Objects;

public class Human {

    private String name;
    private String surname;
    private int year;
    private int iq;
    private Human mather;
    private Human father;
    private Pet pet;
    private String[][] schedule;

    public Human(String name, String surname, int year,int iq) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
    }
    public Human(String name, String surname, int year,int iq, Human mather,Human father) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
        this.mather=mather;
        this.father=father;
    }
    public Human(String name, String surname, int year,int iq, Human mather,Human father,Pet pet) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
        this.mather=mather;
        this.father=father;
        this.pet=pet;
    }
    public Human(String name, String surname, int year, int iq,Pet pet,Human mather,Human father,String[][] schedule) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
        this.pet = pet;
        this.mather = mather;
        this.father = father;
        this.schedule = schedule;
    }
    public Human() {
    }

    public void greetPet(){

        System.out.println("Привіт, "+pet.getNickname());
    }

    public void describePet(){
        if ((pet) !=null) {
            String trick = pet.getTrickLevel() > 50 ? "дуже хитрий" : "майже не хитрий";
            System.out.println("У мене є " + pet.getSpecies() + ", їй " + pet.getAge() + " років, він " + trick + ".");
        } else {
            System.out.println("У мене немає домашнього улюбленця.");
        }
    }

    @Override
    public String toString() {
        return surname + "{" +
                " name='" + name + '\'' +
                ", year=" + year +
                ", ip=" + iq +
                ", mather=" +mather+
                ",father="+father+
                ",pet="+pet+
                "schedule="+Arrays.deepToString(schedule)+
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
       Human human = (Human) o;
        return name.equals(human.name) &&
                surname.equals(human.surname) &&
                year == human.year &&
                iq == human.iq &&
                mather == human.mather&&
                father == human.father&&
                pet == human.pet&&

                Arrays.equals(schedule, human.schedule);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(name, surname, year, iq ,mather,father,pet);
        result = 31 * result + Arrays.hashCode(schedule);
        return result;
    }






    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }


    public void setFamily(Family family) {
    }

    public void setPet(Pet pet) {
    }


}
