package HeppyFamily1;

import java.util.Arrays;
import java.util.Objects;

public class Family {
    private  Human mather;
    private  Human father;
    private Human[] children;
    private Pet pet;

    public Family( Human mather , Human father ,Pet pet ){
        this.mother = mather;
        this.father=father;
        this.children=new Human[0];
        this.pet = pet;
    }
    public Family( Human mather , Human father , Human children){
        this.mother = mather;
        this.father=father;
        this.children=new Human[0];}

    public Family(){}


    public void addChild(Human child) {
        Human[] newChildren = new Human[children.length + 1];
        System.arraycopy(children, 0, newChildren, 0, children.length);
        newChildren[newChildren.length - 1] = child;
        children = newChildren;
        child.setFamily(this);
    }

    public boolean deleteChild(int index) {
        if (index < 0 || index >= children.length) {
            return false;
        }
        Human[] newChildren = new Human[children.length - 1];
        System.arraycopy(children, 0, newChildren, 0, index);
        System.arraycopy(children, index + 1, newChildren, index, children.length - index - 1);
        children = newChildren;
        return true;
    }

    public int countFamily() {
        return children.length + 2;
    }
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Family:").append("\n");
        sb.append("  Mother: ").append(mother).append("\n");
        sb.append("  Father: ").append(father).append("\n");
        sb.append("  Children:").append("\n");
        for (Human child : children) {
            sb.append("    ").append(child).append("\n");
        }
        sb.append("  Pet: ").append(pet).append("\n");
        return sb.toString();
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Family family = (Family) o;
        return mother.equals(family.mother) &&
                father.equals(family.father) &&
                pet == family.pet &&
                Arrays.equals(children, family.children);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(mother, father, pet);
        result = 31 * result + Arrays.hashCode(children);
        return result;
    }








    public Human getMother() {
        return mother;
    }

    private  Human mother;

    public Human getFather() {
        return father;
    }



}
